FROM python:3.10.6-slim-buster AS builder

ENV TZ=UTC

RUN pip install --upgrade pip
RUN pip install poetry
RUN poetry config virtualenvs.create false

WORKDIR /ner_api

COPY ./pyproject.toml /pyproject.toml

RUN poetry lock
RUN poetry install --no-root --without dev,test
RUN poetry run python -m spacy download en_core_web_sm


FROM builder AS project

COPY ./ner_api /ner_api/ner_api/

ENTRYPOINT []

EXPOSE 5000
