SHELL := /bin/bash

py_warn = PYTHONDEVMODE=1

code_dir = ner_api/
tests_dir = tests/
test_modules = $(code_dir)/endpoints,

CMD:=python3 -m


check:
	$(CMD) flake8 $(code_dir); \
	$(CMD) isort -src --check-only $(code_dir) $(tests_dir); \
	$(CMD) black --check $(code_dir) $(tests_dir); \
	$(CMD) xenon -b B -m B -a B $(code_dir);

format:
	$(CMD) isort -src $(code_dir) $(tests_dir); \
	$(CMD) black $(code_dir);

test:
	$(CMD) pytest;
