from fastapi import FastAPI

from ner_api.app import create_app


def test_app_is_fastapi_app():
    """stub test check app is FastAPI instance"""
    assert isinstance(create_app(), FastAPI)
