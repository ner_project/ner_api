import pathlib
import sys

_path = pathlib.Path(__file__).parent.parent.resolve() / "ner_api"
sys.path.append(str(_path))
