"""
    FastAPI application
"""
from endpoints import router
from fastapi import FastAPI


def _build_routers(app: FastAPI) -> None:
    app.include_router(router)


def create_app() -> FastAPI:
    app: FastAPI = FastAPI()
    _build_routers(app)
    return app
