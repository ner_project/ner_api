"""
    Endpoints
"""
from typing import Any

import spacy
from fastapi import APIRouter

from .schemas import DefaultOkResponse, Input, Output

router: APIRouter = APIRouter()


@router.post(
    "/",
    response_model=DefaultOkResponse,
    responses={
        200: {"description": "Запрос успешно обработан"},
    },
)
def some_endpoint() -> Any:
    return DefaultOkResponse()


en_core_web_lg = spacy.load("en_core_web_sm")


@router.post("/extractions", response_model=Output)
def extractions(input_str: Input) -> Any:
    document = en_core_web_lg(input_str.sentence)
    extractions_list = []
    for entity in document.ents:
        extraction = dict()
        extraction["first_index"] = entity.start_char
        extraction["last_index"] = entity.end_char
        extraction["name"] = entity.label_
        extraction["content"] = entity.text
        extractions_list.append(extraction)

    return {"extractions": extractions_list}
