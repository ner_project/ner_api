"""
    Schemas
"""
from typing import List

from pydantic import BaseModel


class DefaultOkResponse(BaseModel):
    message: str = "ok"


class Extraction(BaseModel):
    first_index: int
    last_index: int
    name: str
    content: str


class Output(BaseModel):
    extractions: List[Extraction]


class Input(BaseModel):
    sentence: str
