"""
    Project configuration
"""
from pathlib import Path

from pydantic import BaseSettings


class Config(BaseSettings):
    """
    Configuration class
    """

    base_dir: Path = Path(__file__).parent.parent

    host: str = "0.0.0.0"
    port: int = 5000
    reload: bool = True

    class Config:
        env_prefix = ""
        env_file = ".env"
        env_file_encoding = "utf-8"


config: Config = Config()
