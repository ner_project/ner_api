"""
    Entrypoint
"""
from typing import NoReturn

from config import config


def run_uvicorn() -> NoReturn:
    """Run FatsAPI application over uvicorn server"""
    import uvicorn

    uvicorn.run(
        "app:create_app",
        host=config.host,
        port=config.port,
        reload=config.reload,
        factory=True,
    )


if __name__ == "__main__":
    run_uvicorn()
